# README #

These scripts prepare input files and submit a modified version of qtlBHM python script which outputs priors per SNP (https://github.com/rajanil/qtlBHM). These priors are then used in the coloc model. 
Input for this software are eQTL data and epigenetic annotations in bed format.


### How do I get set up? ###

 Main script: prepare.R. Creates bed files and eQTL data to run single or combined qtlBHM. Combined models include "exclude_Quies", "significant" (also without Quies), "significant_enriched" (without Quies). 
 
* * *
## Main script: prepare.R

"annot": name of annotation to use, e.g. "REMC.coreHMM.imputed"
# THIS IS ONLY FOR REMC
"subset" e.g. TRUE: only use a subset of the REMC annotations, for example those in brain tissue;
"subset_name" e.g. "BRN_DL_PRFRNTL_CRTX"
set = FALSE # if have a specific set of annotations you want to look at
set_names = c("ADD10_NEU_CORRECTED_TF_motif_121", "ADD10_NEU_CORRECTED_TF_motif_315", "ADD10_NEU_CORRECTED_TF_motif_727", "ADD10_NEU_CORRECTED_TF_motif_2675")

singleAnnot=FALSE: This creates a combined bed file for the annotations that resulted significant in the single annotation models (so run this after singleAnnot=TRUE). Combined Models: considering only the annotations with significant weights in the single model (i.e. with Weight +/- (1.96*Stderr) not crossing zero), both enriched and repressed, in the same combined model?

significant = TRUE # this should always be true
significant_enriched = TRUE  # Combine in a single model only annotations that have a positive weight and a 95% CI that does not cross zero


* * *