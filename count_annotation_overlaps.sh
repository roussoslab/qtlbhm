cd /sc/orga/projects/epigenAD/qtlBHM/DLPFC_TFADD10_CORRECTED.ATACSeq/results
for file in $(ls *_variant_annotations.txt.gz); do echo $file; zcat $file | cut -f 2 | sort | uniq -c | tr "\n" " " ; done > ../count_overlaps.txt
